/** 获取签名结果 */
var getSignature = undefined
/** 重签 */
var resetSignature = undefined


jQuery.noConflict();
(function ($) {
  var topics = {};
  $.publish = function (topic, args) {
    if (topics[topic]) {
      var currentTopic = topics[topic], args = args || {};
      for (var i = 0, j = currentTopic.length; i < j; i++) {
        currentTopic[i].call($, args);
      }
    }
  };
  $.subscribe = function (topic, callback) {
    if (!topics[topic]) {
      topics[topic] = [];
    }
    topics[topic].push(callback);
    return {
      "topic": topic,
      "callback": callback
    };
  };
  $.unsubscribe = function (handle) {
    var topic = handle.topic;
    if (topics[topic]) {
      var currentTopic = topics[topic];
      for (var i = 0, j = currentTopic.length; i < j; i++) {
        if (currentTopic[i] === handle.callback) {
          currentTopic.splice(i, 1);
        }
      }
    }
  };
})(jQuery);
(function ($) {
  $(document).ready(function () {
    var $sigdiv = $("#signature")
    .jSignature({
      lineWidth: 1,
      width: '100%', 
      height: '100%', 
      UndoButton: false, 
    });
    // $sigdiv.jSignature({'decor-color':'red'}); // 初始化jSignature插件-设置横线颜色
    // $sigdiv.jSignature({'lineWidth':"1"});// 初始化jSignature插件-设置横线粗细
    // $sigdiv.jSignature({"decor-color":"transparent"});// 初始化jSignature插件-去掉横线
    // $sigdiv.jSignature({'UndoButton':true});// 初始化jSignature插件-撤销功能
    // $sigdiv.jSignature({'height': 100, 'width': 200}); // 初始化jSignature插件-设置书写范围(大小)
    getSignature = function() {
      //可选格式：native,image,base30,image/jsignature;base30,svg,image/svg+xml,svgbase64,image/svg+xml;base64
      const basedata = '' + $sigdiv.jSignature('getData', "svg");
      const svg = basedata.replace('image/svg+xml,', '')
      console.log(svg);
      return svg
    }
    resetSignature = function() {
      $sigdiv.jSignature('reset');
    }
  })
})(jQuery)